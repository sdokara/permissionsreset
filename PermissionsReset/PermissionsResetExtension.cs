﻿using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using SharpShell.Attributes;
using SharpShell.SharpContextMenu;

namespace PermissionsReset
{
    [ComVisible(true)]
    [COMServerAssociation(AssociationType.AllFilesAndFolders)]
    public class PermissionsResetExtension: SharpContextMenu
    {
        protected override bool CanShowMenu()
        {
            return true;
        }

        protected override ContextMenuStrip CreateMenu()
        {
            ContextMenuStrip strip = new ContextMenuStrip();
            ToolStripMenuItem item = new ToolStripMenuItem()
            {
                Text = "Reset permissions"
            };
            item.Click += (sender, args) =>
            {
                new PermissionsResetForm(SelectedItemPaths).ShowDialog();
            };
            strip.Items.Add(item);
            return strip;
        }
    }
}
