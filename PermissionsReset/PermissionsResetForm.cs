﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

namespace PermissionsReset
{
    public partial class PermissionsResetForm : Form
    {
        private readonly IEnumerable<string> paths;

        public PermissionsResetForm(IEnumerable<string> paths)
        {
            this.paths = paths;
            InitializeComponent();

            cboxOwner.Items.AddRange(new object[]
            {
                "Administrators",
                System.Security.Principal.WindowsIdentity.GetCurrent().Name
            });
            cboxOwner.SelectedIndex = 0;
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            btnReset.Enabled = false;
            Cursor = Cursors.WaitCursor;
            foreach (string path in paths)
            {
                if (chkTakeOwnership.Checked)
                {
                    TakeOwnership(path);
                }
                ResetPermissions(path);
            }
            Cursor = Cursors.Default;
            Close();
        }

        private void ResetPermissions(string path)
        {
            try
            {
                Process process = new Process
                {
                    StartInfo = { FileName = "icacls", WindowStyle = ProcessWindowStyle.Hidden }
                };
                if (cboxSystem.Checked)
                {
                    process.StartInfo.Arguments = Quote(path) + " " +
                                                  "/t /c /grant \"NT SERVICE\\TrustedInstaller\":(F) " +
                                                  "/t /c /grant SYSTEM:(M) " +
                                                  "/t /c /grant SYSTEM:(F) " +
                                                  "/t /c /grant Administrators:(M) " +
                                                  "/t /c /grant Administrators:(F) " +
                                                  "/t /c /grant Users:(RX) " +
                                                  "/t /c /grant Users:(GR,GE) " +
                                                  "/t /c /grant \"CREATOR OWNER\":(F)";
                }
                else
                {
                    process.StartInfo.Arguments = Quote(path) + " " +
                                                  "/t /c /grant SYSTEM:(F) " +
                                                  "/t /c /grant Administrators:(F) " +
                                                  "/t /c /grant Users:(RX) " +
                                                  "/t /c /grant Users:(GR,GE) " +
                                                  "/t /c /grant Everyone:(RX) " +
                                                  "/t /c /grant Everyone:(GR,GE)";
                }
                process.Start();
                process.WaitForExit();
                if (process.ExitCode != 0)
                {
                    MessageBox.Show($"Failed processing {path}.", "Reset permissions", MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Failed processing {path}: {ex.Message}.", "Reset permissions", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        private void TakeOwnership(string path)
        {
            try
            {
                Process process = new Process
                {
                    StartInfo = { FileName = "takeown", WindowStyle = ProcessWindowStyle.Hidden }
                };
                process.StartInfo.Arguments = $"/f {Quote(path)}";
                if (IsDirectory(path))
                {
                    process.StartInfo.Arguments += " /R /D Y";
                }
                if (cboxOwner.SelectedIndex == 0)
                {
                    process.StartInfo.Arguments += " /A";
                }
                process.Start();
                process.WaitForExit();
                if (process.ExitCode != 0)
                {
                    MessageBox.Show($"Failed taking over {path}.", "Take ownership", MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Failed taking over {path}: {ex.Message}.", "Take ownership", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        private void chkTakeOwnership_CheckedChanged(object sender, EventArgs e)
        {
            cboxOwner.Enabled = chkTakeOwnership.Checked;
        }

        private static bool IsDirectory(string path)
        {
            FileAttributes attr = File.GetAttributes(path);
            return ((attr & FileAttributes.Directory) == FileAttributes.Directory);
        }

        private static string Quote(string str)
        {
            return $"\"{str}\"";
        }
    }
}
