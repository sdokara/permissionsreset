﻿namespace PermissionsReset
{
    partial class PermissionsResetForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cboxSystem = new System.Windows.Forms.CheckBox();
            this.btnReset = new System.Windows.Forms.Button();
            this.chkTakeOwnership = new System.Windows.Forms.CheckBox();
            this.lblOwner = new System.Windows.Forms.Label();
            this.cboxOwner = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // cboxSystem
            // 
            this.cboxSystem.AutoSize = true;
            this.cboxSystem.Location = new System.Drawing.Point(12, 62);
            this.cboxSystem.Name = "cboxSystem";
            this.cboxSystem.Size = new System.Drawing.Size(174, 17);
            this.cboxSystem.TabIndex = 0;
            this.cboxSystem.Text = "Treat as system directories/files";
            this.cboxSystem.UseVisualStyleBackColor = true;
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(12, 92);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(260, 23);
            this.btnReset.TabIndex = 1;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // chkTakeOwnership
            // 
            this.chkTakeOwnership.AutoSize = true;
            this.chkTakeOwnership.Location = new System.Drawing.Point(12, 12);
            this.chkTakeOwnership.Name = "chkTakeOwnership";
            this.chkTakeOwnership.Size = new System.Drawing.Size(102, 17);
            this.chkTakeOwnership.TabIndex = 2;
            this.chkTakeOwnership.Text = "Take ownership";
            this.chkTakeOwnership.UseVisualStyleBackColor = true;
            this.chkTakeOwnership.CheckedChanged += new System.EventHandler(this.chkTakeOwnership_CheckedChanged);
            // 
            // lblOwner
            // 
            this.lblOwner.AutoSize = true;
            this.lblOwner.Location = new System.Drawing.Point(12, 38);
            this.lblOwner.Name = "lblOwner";
            this.lblOwner.Size = new System.Drawing.Size(38, 13);
            this.lblOwner.TabIndex = 3;
            this.lblOwner.Text = "Owner";
            // 
            // cboxOwner
            // 
            this.cboxOwner.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboxOwner.Enabled = false;
            this.cboxOwner.FormattingEnabled = true;
            this.cboxOwner.Location = new System.Drawing.Point(56, 35);
            this.cboxOwner.Name = "cboxOwner";
            this.cboxOwner.Size = new System.Drawing.Size(216, 21);
            this.cboxOwner.TabIndex = 4;
            // 
            // PermissionsResetForm
            // 
            this.AcceptButton = this.btnReset;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 127);
            this.Controls.Add(this.cboxOwner);
            this.Controls.Add(this.lblOwner);
            this.Controls.Add(this.chkTakeOwnership);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.cboxSystem);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PermissionsResetForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Reset Permissions";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox cboxSystem;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.CheckBox chkTakeOwnership;
        private System.Windows.Forms.Label lblOwner;
        private System.Windows.Forms.ComboBox cboxOwner;
    }
}