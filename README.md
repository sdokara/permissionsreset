### Permissions Reset

Permissions Reset is a context-menu Windows Explorer extension allowing to you 
quickly reset selected items' ownership and security permissions using
`takeown` and `icacls`.

Useful when your `chkdsk` erroneously detects invalid security descriptors and
replaces them with almost blank ones, and you really do not want to reinstall
your system.


### Installation

To install the extension, use [SharpShell](https://github.com/dwmkerr/sharpshell) Server Registration Manager:

- build the project
- go to the build directory (PermissionsReset\bin\Release\)
- to install, run:   `ServerRegistrationManager.exe install PermissionsReset.dll -codebase`
- to uninstall: run: `ServerRegistrationManager.exe uninstall PermissionsReset.dll`


### Usage

Right-click on a file, directory, or custom selection, and choose **Reset permissions**.
You will be presented with a dialog box with the following options:

- Take ownership: takes recursive ownership on the selection in the name of:
    - `Administrators` or
	- your own user

- Treat as system directories/files: when **disabled**:
    - sets F flags for `SYSTEM` and `Administrators`
	- sets RX, GR, and GE flags for `Users`
	- sets RX, GR, and GE flags for `Everyone`
	- applicable for *regular* directories

- Treat as system directories/files: when **enabled**:
	- sets F and M flags for `SYSTEM` and `Administrators`
    - sets F flag for `TrustedInstaller`
	- sets RX, GR, and GE flags for `Users`
	- sets F flag for `CREATOR_OWNER`
	- applicable for **Windows**, **Program Files**, and **Program files (x86)** directories

For more information on permission masks, see [this article](https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/icacls).

### Disclaimer

These are not really the optimal (nor original) permissions, but they should allow you 
to access and work with files normally.

For instance: the owner of the `Program Files` and `Windows` directories is the 
`TrustedInstaller`, `SYSTEM` owns `Users`, and specific users own their own user directories. 
Additionally, some directories do not specify access for the `Users` and `Everyone` groups. 
Having them there ensures read and execute access.
